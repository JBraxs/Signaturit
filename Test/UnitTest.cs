using Adapters.DTOs;
using Adapters.Services;
using AutoMapper;
using Core.Interfaces;
using Core.Models;
using Core.Services;
using Core.Utils;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace UnitTest
{
    public class UnitTest
    {
        private readonly IMapper _mapper;
        private readonly Mock<IContractServices> _contractServicesMock;
        private readonly Mock<ILogger<ContractServices>> _loggerMock;
        private readonly ContractServicesAdapter _contractServicesAdapter;

        public UnitTest()
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContractDTO, Contract>();
            }).CreateMapper();

            _contractServicesMock = new Mock<IContractServices>();
            _loggerMock = new Mock<ILogger<ContractServices>>();

            _contractServicesAdapter = new ContractServicesAdapter(_mapper, _contractServicesMock.Object, _loggerMock.Object);
        }

        [Fact]
        public void ContractValuationTest()
        {
            // Arrange
            var contractDTO = new ContractDTO
            {
                Plaintiff = "KVN",
                Defendant = "KV"
            };

            var expectedWinner = Constants.ConstantsText.PLAINTIFF_WINS;

            // Act
            var result = _contractServicesAdapter.ContractValuation(contractDTO);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedWinner, result.Winner);
        }

        [Fact]
        public void CompareContractsTest()
        {
            // Arrange
            var contractDTO = new ContractDTO
            {
                Plaintiff = "N#V",
                Defendant = "NVV"
            };

            var expectedCompareContractsMessage = string.Empty;

            _contractServicesMock.Setup(mock => mock.AddCompareContracts(It.IsAny<Contract>()));

            // Act
            var result = _contractServicesAdapter.CompareContracts(contractDTO);

            // Assert
            Assert.NotNull(result);
            Assert.Equal("The plaintiff is missing the following signatures to win. N", result.CompareContractsMessage);
            _contractServicesMock.Verify(mock => mock.AddCompareContracts(It.IsAny<Contract>()), Times.Once);
        }
    }
}
