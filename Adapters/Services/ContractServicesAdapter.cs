﻿using Adapters.DTOs;
using Adapters.Interfaces;
using AutoMapper;
using Core.Interfaces;
using Core.Models;
using Core.Services;
using Core.Utils;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;

namespace Adapters.Services
{
    public class ContractServicesAdapter : IContractServicesPort
    {
        private readonly IMapper _mapper;
        private readonly IContractServices _contractServices;
        private readonly ILogger<ContractServices> _logger;

        public ContractServicesAdapter(IMapper mapper, IContractServices contractServices, ILogger<ContractServices> logger)
        {
            _contractServices = contractServices;
            _mapper = mapper;
            _logger = logger;
        }


        public ApiResponse<ContractDTO> ContractValuation(ContractDTO contractDTO)
        {
            _logger.LogInformation("The AddContract method is initiated.");

            ApiResponse<ContractDTO> result = new();
            Contract contract = new();

            try
            {
                result.Message = GenericSignatureValidations(contractDTO);
                if (String.IsNullOrEmpty(result.Message))
                {
                    List<char> partPlaintiff = contractDTO.Plaintiff.ToUpper().ToCharArray().ToList();
                    List<char> partDefendant = contractDTO.Defendant.ToUpper().ToCharArray().ToList();

                    int plaintiffTotalPoints = partPlaintiff.Sum(ch => GetValueSignature(ch));
                    int defendantTotalPoints = partDefendant.Sum(ch => GetValueSignature(ch));

                    //We understand that there cannot be a tie in this process.
                    if (plaintiffTotalPoints > defendantTotalPoints)
                        result.Winner = Constants.ConstantsText.PLAINTIFF_WINS;
                    else
                        result.Winner = Constants.ConstantsText.DEFENDANT_WINS;

                    contract = _mapper.Map<Contract>(contractDTO);
                    contract.PlaintiffTotalPoints = plaintiffTotalPoints;
                    contract.DefendantTotalPoints = defendantTotalPoints;
                    contract.Winner = result.Winner;
                    _contractServices.AddContract(contract);

                    result.Data = contractDTO;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while trying to contract evaluation, ContractValuation method.");
                throw new Exception(Constants.ConstantsText.ERROR_GENERIC);
            }

            return result;
        }

        public ApiResponse<ContractDTO> CompareContracts(ContractDTO contractDTO)
        {
            _logger.LogInformation("The CompareContracts method is initiated.");

            ApiResponse<ContractDTO> result = new();
            Contract contract = new();
            string responseSign = string.Empty;
            int plaintiffTotalPoints = 0;
            int defendantTotalPoints = 0;

            try
            {
                result.Message = GenericSignatureValidations(contractDTO);
                if (String.IsNullOrEmpty(result.Message))
                {
                    //We validate that only one of the two contracts has a "#" signature.
                    if ((contractDTO.Plaintiff.Contains(Constants.ConstantsText.NOT_FIRMA) && !contractDTO.Defendant.Contains(Constants.ConstantsText.NOT_FIRMA)) ||
                        (!contractDTO.Plaintiff.Contains(Constants.ConstantsText.NOT_FIRMA) && contractDTO.Defendant.Contains(Constants.ConstantsText.NOT_FIRMA)))
                    {
                        List<char> partPlaintiff = contractDTO.Plaintiff.ToUpper().ToCharArray().ToList();
                        List<char> partDefendant = contractDTO.Defendant.ToUpper().ToCharArray().ToList();

                        //Case with King signs included.
                        if (partPlaintiff.Contains(Constants.ConstantsText.KING))
                            plaintiffTotalPoints = partPlaintiff.Where(item => item != Constants.ConstantsText.VALIDATOR).Sum(ch => GetValueSignature(ch));
                        else
                            plaintiffTotalPoints = partPlaintiff.Sum(ch => GetValueSignature(ch));

                        if (partDefendant.Contains(Constants.ConstantsText.KING))
                            defendantTotalPoints = partDefendant.Where(item => item != Constants.ConstantsText.VALIDATOR).Sum(ch => GetValueSignature(ch));
                        else
                            defendantTotalPoints = partDefendant.Sum(ch => GetValueSignature(ch));


                        /*Missing signature case, which signature is required to win.
                        We compare the total points obtained to check if it is necessary or not to return the missing signature to win.
                        If it is not necessary, return the missing signature to win; otherwise, return that the victory has already been achieved.*/
                        if (contractDTO.Plaintiff.Contains(Constants.ConstantsText.NOT_FIRMA))
                        {
                            if (plaintiffTotalPoints < defendantTotalPoints)
                            {
                                //We return the required signatures to win.
                                MissingPointsToWin(defendantTotalPoints - plaintiffTotalPoints,
                                   contractDTO.Plaintiff.Contains(Constants.ConstantsText.KING), ref responseSign);

                                responseSign = Constants.ConstantsText.PLAINTIFF_MISSING + responseSign;
                            }
                            else
                                result.Winner = Constants.ConstantsText.PLAINTIFF_WINS;
                        }
                        else if (contractDTO.Defendant.Contains(Constants.ConstantsText.NOT_FIRMA))
                        {
                            if (defendantTotalPoints < plaintiffTotalPoints)
                            {
                                //We return the required signatures to win.
                                MissingPointsToWin(plaintiffTotalPoints - defendantTotalPoints,
                                    contractDTO.Plaintiff.Contains(Constants.ConstantsText.KING), ref responseSign);

                                responseSign = Constants.ConstantsText.DEFENDANT_MISSING + responseSign;
                            }
                            else
                                result.Winner = Constants.ConstantsText.DEFENDANT_WINS;
                        }
                        else
                        {
                            if (plaintiffTotalPoints > defendantTotalPoints)
                                result.Winner = Constants.ConstantsText.PLAINTIFF_WINS;
                            else
                                result.Winner = Constants.ConstantsText.DEFENDANT_WINS;
                        }
                    }

                    result.CompareContractsMessage = responseSign;
                    contract = _mapper.Map<Contract>(contractDTO);
                    contract.PlaintiffTotalPoints = plaintiffTotalPoints;
                    contract.DefendantTotalPoints = defendantTotalPoints;
                    contract.CompareContractsMessage = result.CompareContractsMessage;
                    _contractServices.AddCompareContracts(contract);

                    result.Data = contractDTO;
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while trying to save the contract, ContractValuation method.");
                throw new Exception(Constants.ConstantsText.ERROR_GENERIC);
            }
        }

        private static int GetValueSignature(char role)
        {
            return role switch
            {
                Constants.ConstantsText.KING => (int)Constants.RolesValues.KING_K,
                Constants.ConstantsText.NOTARY => (int)Constants.RolesValues.NOTARY_N,
                Constants.ConstantsText.VALIDATOR => (int)Constants.RolesValues.VALIDATOR_V,
                _ => 0,
            };
        }

        private static string GenericSignatureValidations(ContractDTO contractDTO)
        {
            //Generic signature validations for the contract.
            #region business logic 
            /*Validate that it contains at least one of the characters that we understand are mandatory to continue.
            And that those characters are one of KNV# and never come empty.*/
            string message = Constants.ConstantsText.PLAINTIFF_DEFENDANT_REQUIRED;
            if (!String.IsNullOrEmpty(contractDTO.Plaintiff) && !String.IsNullOrEmpty(contractDTO.Defendant))
            {
                contractDTO.Plaintiff = contractDTO.Plaintiff.Trim();
                contractDTO.Defendant = contractDTO.Defendant.Trim();

                message = Constants.ConstantsText.INCORRECT_SIGNATURES;
                /*We validate the content to match the regular expression, understanding that only these KNV# characters can be entered, 
                and there can only be one occurrence of this # signature or no # at all.*/
                string pattern = @"^(?=[kKvVnN#]*$)(?!.*#.*#)[kKvVnN#]*$";

                if (Regex.IsMatch(contractDTO.Plaintiff, pattern) && (Regex.IsMatch(contractDTO.Defendant, pattern)))
                    //Once the validations are completed, the variable "Message" is emptied.
                    message = string.Empty;
            }
            #endregion

            return message;
        }

        private static void MissingPointsToWin(int resultPoints, bool isKing, ref string sign)
        {
            //If the condition being validated contains a king, V cannot be returned.
            //If the difference is equal to or greater than -1, it is understood that the necessary points to win have been surpassed.
            if (resultPoints >= 0)
            {
                if (resultPoints == 0)
                {
                    //If the signature contains a king, we pass an N, and if it doesn't contain a king, winning is with V.
                    if (isKing)
                    {
                        sign += Constants.ConstantsText.NOTARY;
                        resultPoints -= (int)Constants.RolesValues.NOTARY_N;
                    }
                    else
                    {
                        sign += Constants.ConstantsText.VALIDATOR;
                        resultPoints -= (int)Constants.RolesValues.VALIDATOR_V;
                    }
                }
                else if (resultPoints == 1)
                {
                    //When there is a difference of only one point, whether or not it is a king, we return N.
                    sign += Constants.ConstantsText.NOTARY;
                    resultPoints -= (int)Constants.RolesValues.NOTARY_N;
                }
                else if (resultPoints > 1)
                {
                    //We consider the case that there cannot be more than one king per contract.
                    sign += Constants.ConstantsText.NOTARY;
                    resultPoints -= (int)Constants.RolesValues.NOTARY_N;
                }

            }

            if (resultPoints >= 0)
                MissingPointsToWin(resultPoints, isKing, ref sign);
            else
                return;
        }
    }
}
