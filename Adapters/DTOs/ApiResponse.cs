﻿namespace Adapters.DTOs
{
    public class ApiResponse<T>
    {
        public T Data { get; set; }
        public string? Message { get; set; }
        public string? Winner { get; set; }
        public string? CompareContractsMessage { get; set; }
    }
}
