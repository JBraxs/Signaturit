﻿namespace Adapters.DTOs
{
    public class ContractDTO
    {
        public string Plaintiff { get; set; }
        public string Defendant { get; set; }

        public ContractDTO()
        {
            Plaintiff = string.Empty;
            Defendant = string.Empty;
        }
    }
}
