﻿using Adapters.DTOs;
using AutoMapper;
using Core.Models;

namespace Adapters.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Contract, ContractDTO>()
                .ReverseMap();
        }
    }
}
