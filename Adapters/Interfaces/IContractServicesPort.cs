﻿using Adapters.DTOs;

namespace Adapters.Interfaces
{
    public interface IContractServicesPort
    {
        ApiResponse<ContractDTO> ContractValuation(ContractDTO contractDTO);

        ApiResponse<ContractDTO> CompareContracts(ContractDTO contractDTO);
    }
}
