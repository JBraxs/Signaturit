﻿using Core.Interfaces;
using Core.Models;

namespace Adapters.Repositories
{
    public class BaseRepository : IRepository
    {
        //Class with access to the data layer, since we don't have it we use the Contract entity, this would have to be a generic class.
        private readonly List<Contract> _contract;

        public BaseRepository()
        {
            _contract = new List<Contract>();
        }

        public IEnumerable<Contract> GetAll()
        {
            return _contract.AsEnumerable();
        }

        public Contract GetById(string id)
        {
            return _contract.FirstOrDefault(x => x.ID == id);
        }

        public void Add(Contract entity)
        {
            _contract.Add(entity);
        }

        public void Update(Contract entity)
        {
            var existEntity = _contract.FirstOrDefault(p => p.ID == entity.ID);
            if (existEntity != null)
            {
                existEntity.Defendant = entity.Defendant;
                existEntity.Plaintiff = entity.Plaintiff;
            }
        }

        public void Remove(Contract entity)
        {
            _contract.Remove(entity);
        }
    }
}
