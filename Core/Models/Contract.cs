﻿namespace Core.Models
{
    public class Contract
    {
        public string ID { get; set; }
        public string Plaintiff { get; set; }
        public string Defendant { get; set; }
        public int PlaintiffTotalPoints { get; set; }
        public int DefendantTotalPoints { get; set; }
        public string? Winner { get; set; }
        public string? CompareContractsMessage { get; set; }

        public Contract()
        {
            ID = string.Empty;
            Plaintiff = string.Empty;
            Defendant = string.Empty;
        }
    }
}
