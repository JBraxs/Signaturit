﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using Microsoft.Extensions.Logging;

namespace Core.Services
{
    public class ContractServices : IContractServices
    {
        private readonly IRepository _repository;
        private readonly ILogger<ContractServices> _logger;

        public ContractServices(IRepository repository, ILogger<ContractServices> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public Contract AddContract(Contract contract)
        {
            _logger.LogInformation("The AddContract method is initiated.");
            try
            {
                if (contract != null)
                {
                    //We generate a GUID to simulate the saving in the database, thus performing an "audit".
                    contract.ID = Guid.NewGuid().ToString();
                    _repository.Add(contract);
                    
                    //We simulate the saving process and an audit through logs in this case.
                    _logger.LogInformation($"The following contract has been successfully saved. Contract ID: " +
                        $"{contract.ID}, Plaintiff: {contract.Plaintiff}, Defendant: {contract.Defendant}, Plaintiff Total Points: " +
                        $"{contract.PlaintiffTotalPoints}, Defendant Total Points: {contract.DefendantTotalPoints}, Winner: {contract.Winner}");
                }

                return contract;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while trying to save the contract, AddContract method.");
                throw new Exception(Constants.ConstantsText.ERROR_GENERIC);
            }
        }

        public Contract AddCompareContracts(Contract contract) 
        {
            _logger.LogInformation("The AddCompareContracts method is initiated.");
            try
            {
                if (contract != null)
                {
                    //We generate a GUID to simulate the saving in the database, thus performing an "audit".
                    contract.ID = Guid.NewGuid().ToString();
                    _repository.Add(contract);

                    //We simulate the saving process and an audit through logs in this case.
                    _logger.LogInformation($"The signature comparison data has been successfully saved. Contract ID: " +
                        $"{contract.ID}, Plaintiff: {contract.Plaintiff}, Defendant: {contract.Defendant}, Signatures required to win.: " +
                        $"{contract.CompareContractsMessage}");
                }

                return contract;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while trying to save the signature comparison, AddCompareContracts method.");
                throw new Exception(Constants.ConstantsText.ERROR_GENERIC);
            }
        }
    }
}
