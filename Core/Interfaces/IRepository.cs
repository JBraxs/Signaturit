﻿using Core.Models;

namespace Core.Interfaces
{
    //This should be a generic repository, but we use the contract entity
    public interface IRepository
    {
        IEnumerable<Contract> GetAll();
        Contract GetById(string id);
        void Add(Contract entity);
        void Update(Contract entity);
        void Remove(Contract entity);
    }
}
