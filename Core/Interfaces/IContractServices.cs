﻿using Core.Models;

namespace Core.Interfaces
{
    public interface IContractServices
    {
        Contract AddContract(Contract contract);
        Contract AddCompareContracts(Contract contract);
    }
}
