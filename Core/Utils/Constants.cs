﻿namespace Core.Utils
{
    public static class Constants
    {
        public class ConstantsText
        {
            //Generic Messages
            public const string PLAINTIFF_DEFENDANT_REQUIRED = "Plaintiff and Defendant are required.";
            public const string INCORRECT_SIGNATURES = "Incorrect signatures do not match the correct format.";
            public const char KING = 'K';
            public const char NOTARY = 'N';
            public const char VALIDATOR = 'V';
            public const char NOT_FIRMA = '#';

            public const string PLAINTIFF_WINS = "The plaintiff wins the lawsuit.";
            public const string DEFENDANT_WINS = "The defendant wins the lawsuit.";
            public const string PLAINTIFF_MISSING = "The plaintiff is missing the following signatures to win. ";
            public const string DEFENDANT_MISSING = "The defendant is missing the following signatures to win. ";

            //Error messages
            public const string ERROR_GENERIC = "An error has occurred. Please try again or contact support.";
        }

        //Enums Roles Points
        public enum RolesValues
        {
            KING_K = 5,
            NOTARY_N = 2,
            VALIDATOR_V = 1
        }
    }
}
