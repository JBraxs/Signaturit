using Adapters.DTOs;
using Adapters.Interfaces;
using Adapters.Mapping;
using Adapters.Repositories;
using Adapters.Services;
using Core.Interfaces;
using Core.Services;

var builder = WebApplication.CreateBuilder(args);

//Service configuration
builder.Services.AddTransient<IContractServices, ContractServices>();
builder.Services.AddTransient<IRepository, BaseRepository>();
builder.Services.AddTransient<IContractServicesPort, ContractServicesAdapter>();
builder.Services.AddAutoMapper(typeof(MappingProfile));

//Swagger Configuration
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

//Swagger enable
app.UseSwagger();
app.UseSwaggerUI();

app.Logger.LogInformation("Init Aplicaction");

//Endpoint Swagger - start application
app.MapGet("/", () => Results.Redirect("/swagger/index.html"))
    .ExcludeFromDescription();


app.MapPost("/contractValuation", (ContractDTO contractDTO, IContractServicesPort contractServicesPort) =>
{
    app.Logger.LogInformation("Post Endpoint - ContractValuation Method");
    ApiResponse<ContractDTO> response = contractServicesPort.ContractValuation(contractDTO);

    if (String.IsNullOrEmpty(response.Message))
        return Results.Ok(response);
    else
        return Results.BadRequest(response.Message);
});

app.MapPost("/compareContracts", (ContractDTO contractDTO, IContractServicesPort contractServicesPort) =>
{
    app.Logger.LogInformation("Post Endpoint - compareContracts Method");
    ApiResponse<ContractDTO> response = contractServicesPort.CompareContracts(contractDTO);

    if (String.IsNullOrEmpty(response.Message))
        return Results.Ok(response);
    else
        return Results.BadRequest(response.Message);
});


app.Run();
